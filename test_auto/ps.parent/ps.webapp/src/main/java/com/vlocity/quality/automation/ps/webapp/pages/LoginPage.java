/** Interface exposing the Login page services.
 * 
 */
package com.vlocity.quality.automation.ps.webapp.pages;

import org.openqa.selenium.WebDriver;

/** This is the Login page for Saleforce web application
 * @author npaila
 *
 */
public interface LoginPage {
	
	/** Performs Login using the passed in credentials expecting success.
	 * 
	 * @param username User name of the user trying to use the application
	 * @param password Password of the user
	 * @return Main application page after successful login
	 */
//	public HomePage loginAs(String username, String password);
	public WebDriver loginAs(String username, String password);
	
//  ... failed login here, maybe because one or both of the username and password are wrong
	
    /** Performs Login using the passed in credentials expecting failure
     * 
	 * @param username User name of the user trying to use the application
	 * @param password Password of the user
     * @return LoginPage
     */
    public LoginPage loginAsExpectingError(String username, String password);

}
