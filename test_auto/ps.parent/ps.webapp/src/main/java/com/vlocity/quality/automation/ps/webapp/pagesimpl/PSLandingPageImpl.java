/**
 * 
 */
package com.vlocity.quality.automation.ps.webapp.pagesimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.vlocity.quality.automation.ps.webapp.pages.PSLandingPage;;

/**
 * @author npaila
 * 
 */
public class PSLandingPageImpl implements PSLandingPage {
	private final WebDriver driver;
	private List<WebElement> tabLinks;
//	private Map<String, WebElement> tabLinkMap;
	private Map<String, String> tabLinkMap;

	public PSLandingPageImpl(WebDriver driver) {
		this.driver = driver;
		captureTabLinks();
	}
	
	public synchronized void captureTabLinks() {
		refreshTabLinks();
	}

	/** Identifies all the tabs exposed by the application, the corresponding links and populates them
	 * in tabLinkMap.
	 * 
	 * @param driver
	 */
	private void refreshTabLinks() {
		// Fetch all the tabs and store them
//		tabLinks = driver.findElements(By.xpath("//ul[@id='tabBar']/li/a"));
		tabLinks = driver.findElements(By.xpath("//ul[@id='tabBar']/li/a[@title]"));
		System.out.println("tablinks :: " + tabLinks);
		
//		tabLinkMap = new HashMap<String, WebElement>();
		tabLinkMap = new HashMap<String, String>();
		int tabIndex = 0;
		for (WebElement tab : tabLinks) {
//			tabLinkMap.put(tab.getText(), tab);
			
			String tabLinkWithTitle = "//ul[@id='tabBar']/li/a[@title='" + tab.getAttribute("title") + "']";
			tabLinkMap.put(tab.getText(), tabLinkWithTitle);
		}
	}
	
	//ABSTRACT CLASS SHOULD NOT PROVIDE IMPLEMENTATION FOR THIS*
/*	public void switchToTab(FinancialServicesTabs tab) {
		switch(tab) {
			case Clients:
				tabLinkMap.get("Clients").click();
				break;
			
		}
	}*/

	/* (non-Javadoc)
	 * @see com.vlocity.qe.testauto.core.webui.pages.HomePage#getTabLinkMap()
	 */
	/*public Map<String, WebElement> getTabLinkMap() {
		return tabLinkMap;
	}*/
	public Map<String, String> getTabLinkMap() {
		return tabLinkMap;
	}

}
