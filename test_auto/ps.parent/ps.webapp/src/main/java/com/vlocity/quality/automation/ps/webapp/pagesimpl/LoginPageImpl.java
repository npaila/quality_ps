/**
 * 
 */
package com.vlocity.quality.automation.ps.webapp.pagesimpl;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.vlocity.quality.automation.ps.webapp.pages.LoginPage;

/**
 * @author npaila
 *
 */
public class LoginPageImpl implements LoginPage {
	private final WebDriver driver;
	
	// Element Locators
// EVALUATE USAGE OF @FindBys @FindBy ByChained
	By usernameLocator = By.id("username");
	By passwordLocator = By.id("password");
	By loginButtonLocator = By.id("Login");
	
	public LoginPageImpl(WebDriver driver) {
		this.driver = driver;
		
		// Check that we're on the right page.
        if (!"salesforce.com - Customer Secure Login Page".equals(driver.getTitle())) {
            throw new IllegalStateException("This is not the login page");
        }
	}
	
	/* (non-Javadoc)
	 * @see com.vlocity.qe.testauto.core.webui.pages.LoginPage#loginAs(java.lang.String, java.lang.String)
	 */
	public WebDriver loginAs(String username, String password) {
		driver.findElement(usernameLocator).sendKeys(username);
		driver.findElement(passwordLocator).sendKeys(password);
		driver.findElement(loginButtonLocator).submit();
		
		// MODIFY TO RETURN DRIVER INSTEAD OF PAGE
//		return new HomePageImpl(driver);
		return driver;
	}

	/* (non-Javadoc)
	 * @see com.vlocity.qe.testauto.core.webui.pages.LoginPage#loginAsExpectingError(java.lang.String, java.lang.String)
	 */
	public LoginPage loginAsExpectingError(String username, String password) {
		return null;
	}

}
