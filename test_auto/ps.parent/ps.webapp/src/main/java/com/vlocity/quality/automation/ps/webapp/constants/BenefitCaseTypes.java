/**
 * 
 */
package com.vlocity.quality.automation.ps.webapp.constants;

/** Different types of Benefit Cases that can be created using PublicSector application.
 * @author npaila
 *
 */
public enum BenefitCaseTypes {
	CHILDCARE, DISABILITY_ASSISTANCE, EMPLOYMENT, FOOD_ASSISTANCE, HOUSING,
	IMMIGRATION, MEDICAL, EDUCATION
}
