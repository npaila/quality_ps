/** Interface for the services exposed by the landing page for PS application.
 * 
 */
package com.vlocity.quality.automation.ps.webapp.pages;

import java.util.Map;
import org.openqa.selenium.WebElement;


/** Page displayed to the user after successful login to the PS App.
 * NOTE: This likely would vary across verticals and most probably should be extended in verticals.
 *  
 * @author npaila
 *
 */
public interface PSLandingPage {
	
	/** Returns the mapping of tab name and the corresponding link.
	 * @return
	 */
//	Map<String, WebElement> getTabLinkMap();
	Map<String, String> getTabLinkMap();
	
	/** Switch to the Tab passed in.
	 * 
	 */
//	void switchToTab(FinancialServicesTabs tab);
	
	
	/*
	Core vs Vertical
	
	Client
	------
	switchToTab(Client)
	verify_that_user_can_navigate_to_client_Tab
	
	verify_that_client_entries_are_displayed_in_client_Tab
	verify_that_client_DETAILS_are_displayed_in_client_Tab (DETAILED FIELDS - COLUMNS PER ENTRY)
	verify_that_user_can_switch_to_relationships_from_clientsVIEW_in_client_Tab
	verify_that_relationship_entries_are_displayed_in_client_Tab
	verify_that_client_RELATIONSHIPS_are_displayed_in_client_Tab (DETAILED FIELDS - COLUMNS PER ENTRY)
	verify_that_user_can_switch_to_clients_from_relationshipsVIEW_in_client_Tab
	verify_that_user_can_navigate_to_contact_from_client_Tab
	
	Contact
	-------
	verify_that_all_subsections_are_displayed_in_client360
	verify_that_contact_detail_section_is_displayed_correctly_in_client360
	verify_that_KYC_section_is_displayed_correctly_in_client360
	verify_that_FinancialAccounts_section_is_displayed_correctly_in_client360
	verify_that_SecuritiesHoldings_section_is_displayed_correctly_in_client360
	verify_that_Households_section_is_displayed_correctly_in_client360
	verify_that_OpenActivities_section_is_displayed_correctly_in_client360
	verify_that_ActivityHistory_section_is_displayed_correctly_in_client360
	verify_that_Notes&Attachments_section_is_displayed_correctly_in_client360
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	*/
}
