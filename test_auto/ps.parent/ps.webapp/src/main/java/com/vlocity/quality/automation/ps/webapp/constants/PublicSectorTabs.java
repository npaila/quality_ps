/**
 * 
 */
package com.vlocity.quality.automation.ps.webapp.constants;

/**
 * @author npaila
 *
 */
public enum PublicSectorTabs {
	APPLICATIONS, APPLICATIONINTAKE, BENEFITCASES, CASES, HOUSEHOLDS, SERVICEPLANS
}
