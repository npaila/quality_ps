package com.vlocity.quality.automation.ps.webapp.tests;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.vlocity.quality.automation.ps.webapp.pages.LoginPage;
import com.vlocity.quality.automation.ps.webapp.pagesimpl.LoginPageImpl;
import com.vlocity.quality.automation.ps.webapp.constants.PublicSectorTabs;
import com.vlocity.quality.automation.ps.webapp.pagesimpl.PSLandingPageImpl;
import com.vlocity.quality.automation.ps.webapp.constants.BenefitCaseTypes;

public class PSLandingPageTest {
	Logger logger = LoggerFactory.getLogger(PSLandingPageTest.class);
	private WebDriver driver;
	private FluentWait<WebDriver> driverWait;
	private PSLandingPageImpl psLandingPage = null;
	private final String[] expectedTabNames = {"Applications", "Cases", "Benefit Cases", "Households", "Service Plans"};
	private HashMap<PublicSectorTabs, String> tabPageTypes = null;
	
	public PSLandingPageTest() {
		//Initializes pagetypes for all tabs
		tabPageTypes = new HashMap<>();
		tabPageTypes.put(PublicSectorTabs.APPLICATIONS, "Applications");
		tabPageTypes.put(PublicSectorTabs.BENEFITCASES, "Benefit Cases");
		tabPageTypes.put(PublicSectorTabs.CASES, "Cases");
		tabPageTypes.put(PublicSectorTabs.HOUSEHOLDS, "Households");
		tabPageTypes.put(PublicSectorTabs.SERVICEPLANS, "Service Plans");
	}
		
	@BeforeTest
	public void init() {
		driver = new FirefoxDriver();
		
		driverWait = new FluentWait<WebDriver>(driver)
				.withTimeout(45, TimeUnit.SECONDS).pollingEvery(15, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
	}
/*
	@AfterTest
	public void cleanup() {
		if (driver != null) {
			driver.close();
		}
	}
*/
	@Test(enabled=true)
	public void verify_that_login_succeeds_with_valid_credentials() {
		driver.get("https://login.salesforce.com");		
		LoginPage loginPage = new LoginPageImpl(driver);
		loginPage.loginAs("psv4@vlocity.com", "425Market");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertTrue(driver.getTitle().startsWith("Force.com Home Page"), "Login did not succeed");
	}
	
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"}, enabled=true)
	public void verify_that_all_expected_Tabs_for_profile_are_displayed() {
		psLandingPage = new PSLandingPageImpl(driver);
		
		psLandingPage.getTabLinkMap().keySet().containsAll(Arrays.asList(expectedTabNames));
		
		for (String tabName : psLandingPage.getTabLinkMap().keySet()) {
			logger.info(tabName);
		}
	}

	@DataProvider(name="tabs")
	public Object[][] tabPagetypes() {
		Object[][] tabs = new Object[tabPageTypes.size()][1];
		int i = 0;
		
		for (PublicSectorTabs tab : tabPageTypes.keySet()) {
			tabs[i][0] = tab;
			i++;
		}
		
		return tabs;
	}
	
	/**
	 * Test to verify that user can navigate to all applicable tabs in the web application.
	 * @param tab Tab within the PS application.
	 */
	@Test(dataProvider="tabs", dependsOnMethods = {"verify_that_all_expected_Tabs_for_profile_are_displayed"}, enabled=false)
	public void verify_that_user_can_navigate_to_all_tabs(PublicSectorTabs tab){
		switchToTab(tab);
		
//		FluentWait<WebDriver> driverWait = new FluentWait<WebDriver>(driver)
//				.withTimeout(40, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.SECONDS)
//				.ignoring(NoSuchElementException.class);
		
		driverWait.until(ExpectedConditions.presenceOfElementLocated(By.className("pageType")));
		
		String pageType = driver.findElement(By.className("pageType")).getText();
		logger.info("pageType = " + pageType);
		Assert.assertEquals(pageType, tabPageTypes.get(tab), "Either you did not navigate to the right tab or the page is named wrong.");
	}
	
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"})
	public void verify_that_user_can_navigate_to_BenefitCases_Tab() {
		psLandingPage = new PSLandingPageImpl(driver);
		switchToTab(PublicSectorTabs.BENEFITCASES);
		
		String pageType = driver.findElement(By.className("pageType")).getText();
		logger.info("pageType = " + pageType);
		Assert.assertEquals(pageType, "Benefit Cases", "Either you did not navigate to the right tab or the page is named wrong.");
	}
	
	@Test(dependsOnMethods = {"verify_that_user_can_navigate_to_BenefitCases_Tab"})
	public void verify_that_user_can_start_creation_of_new_BenefitCase() {
		driver.findElement(By.name("new")).click();
		
		String pageDescription = driver.findElement(By.className("pageDescription")).getText();
		logger.info("pageDescription = " + pageDescription);
		Assert.assertEquals(pageDescription, "New Benefit Case", "Application ended up in wrong location.");
	}
	
	@DataProvider(name="BenefitCaseTypes")
	private Object[][] getBenefitCaseTypes(){
		Object[][] caseTypes = new Object[8][1];
		int rowIndex = 0;
		
//		System.out.println(BenefitCaseTypes.values());
	
		for (BenefitCaseTypes caseType : BenefitCaseTypes.values()) {
			Object[] row = {caseType};
			caseTypes[rowIndex++] = row;
		}
		
		return caseTypes;
	}
	
	@Test(dataProvider="BenefitCaseTypes", dependsOnMethods = {"verify_that_user_can_start_creation_of_new_BenefitCase"})
	public void verify_that_new_BenefitCase_can_be_created(BenefitCaseTypes benefitCaseType) {
		System.out.println("benefitCaseType = " + benefitCaseType.toString());
		StringBuffer beneficiaryName = new StringBuffer("Auto User_");
		String typeOptionValue = null;
		String localDateTime = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		
		switch (benefitCaseType) {
			case CHILDCARE:
				typeOptionValue = "Child Care";
				break;
			case DISABILITY_ASSISTANCE:
				typeOptionValue = "Disability Assistance";
				break;
			case EMPLOYMENT:
				typeOptionValue = "Employment";
				break;
			case FOOD_ASSISTANCE:
				typeOptionValue = "Food Assistance";
				break;
			case HOUSING:
				typeOptionValue = "Housing";
				break;
			case IMMIGRATION:
				typeOptionValue = "Immigration";
				break;
			case MEDICAL:
				typeOptionValue = "Medical";
				break;
			case EDUCATION:
				typeOptionValue = "Education";
				break;
			default:
				break;
		}
		
		beneficiaryName.append(typeOptionValue);
		beneficiaryName.append("_");
		beneficiaryName.append(localDateTime);
		
//		WebDriverWait driverWait = new WebDriverWait(driver, 4);
//		WebElement nameField = driver.findElement(By.id("Name"));
//		driverWait.until(ExpectedConditions.visibilityOf(nameField));
		
		driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("Name")));
		WebElement nameField = driver.findElement(By.id("Name"));
		
//		driver.findElement(By.id("Name")).sendKeys(beneficiaryName);
		nameField.sendKeys(beneficiaryName);
		
		WebElement type = driver.findElement(By.xpath("//table/tbody/tr[3]/td[2]//select"));
		new Select(type).selectByValue(typeOptionValue);
		WebElement stage = driver.findElement(By.xpath("//table/tbody/tr[3]/td[4]//select"));
		new Select(stage).selectByValue("Intake"); //HARDCODED VALUE
		
//		WebElement servicePlanPrice = driver.findElement(By.xpath("//table/tbody/tr[6]/td[4]//input"));
//		servicePlanPrice.sendKeys("123.45"); //HARDCODED VALUE
				
		WebElement description = driver.findElement(By.xpath("//table[@class='detailList']/tbody/tr[1]/td[2]/textarea"));
		description.sendKeys(beneficiaryName + " --- " + typeOptionValue);
		
		driver.findElement(By.name("save_new")).click();
		/*
		String pageDescription = driver.findElement(By.className("pageDescription")).getText();
		System.out.println("pageDescription = " + pageDescription);
//		Assert.assertEquals(pageDescription, beneficiaryName, "Either you did not navigate to the right tab or the page is named wrong.");
		
		// Initiate creation of new Benefit Case
		switchToTab(PublicSectorTabs.BENEFITCASES);
		driver.findElement(By.name("new")).click();
		*/
	}
	
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"}, enabled=false)
	public void verify_that_application_can_be_submitted() {
		// Select Application Intake tab
		
		// Select Case Application Type (Multiple Types)
		
		// Capture Applicant information (Add another Applicant, US citizen or not)
		
		// Capture Income Details (Different Income types, Add Another Income Type) 
		
		// Capture Expense Details (Different Expense types, Add Another Expense Type)
		
		// Capture Assets Details (Different Asset types, Add Another Asset)
		
		// Running Screen Rules*
		
		// HOW TO VERIFY THAT THE APPLICATION HAS BEEN SUBMITTED?
		
		
		/*
		IWebElement dropDownListBox = driver.findElement(By.Id("selection"));
		SelectElement clickThis = new SelectElement(dropDownListBox);
		clickThis.SelectByText("Germany");
		*/
		
		WebElement userDropDownList = driver.findElement(By.xpath("/html/body/div/div[1]/table/tbody/tr/td[2]/div/div[2]/div/div/div[1]/span"));
		
//		WebElement userDropDownList = driver.findElement(By.id("userNavButton"));
		Select selection = new Select(userDropDownList);
		selection.selectByVisibleText("Setup");
		
		
	}
	
	

    
    

	
	
	/** It gathers links to all the Tabs present and Clicks on the tab passed in.
	 * @param tab PublicSector tab to switch to.
	 */
	private void switchToTab(PublicSectorTabs tab) {
		psLandingPage.captureTabLinks();
		String tabLink = null;
//		WebDriverWait driverWait = new WebDriverWait(driver, 45, 15);
//		driverWait.ignoring(NoSuchElementException.class);
		
//		FluentWait<WebDriver> driverWait = new FluentWait<WebDriver>(driver)
//				.withTimeout(40, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.SECONDS)
//				.ignoring(NoSuchElementException.class);
		
		logger.info("Tab passed in : " + tab);
		
		switch(tab) {
			case APPLICATIONS:
				tabLink = psLandingPage.getTabLinkMap().get("Applications");
				break;
			case APPLICATIONINTAKE:
				tabLink = psLandingPage.getTabLinkMap().get("Application Intake");
				break;	
			case CASES:
				tabLink = psLandingPage.getTabLinkMap().get("Cases");
				break;	
			case BENEFITCASES:
				tabLink = psLandingPage.getTabLinkMap().get("Benefit Cases");
				break;
			case HOUSEHOLDS:
				tabLink = psLandingPage.getTabLinkMap().get("Households");
				break;
			case SERVICEPLANS:
				tabLink = psLandingPage.getTabLinkMap().get("Service Plans");
				break;
		}
		
		logger.info("TabLinkMap : " + psLandingPage.getTabLinkMap());
		logger.info("tabLink : " + tabLink);

//		driverWait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(tabLink)))).click();
		driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabLink))).click();
	}
	
}
