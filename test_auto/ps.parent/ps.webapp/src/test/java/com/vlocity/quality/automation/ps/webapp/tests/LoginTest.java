package com.vlocity.quality.automation.ps.webapp.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.vlocity.quality.automation.ps.webapp.pages.LoginPage;
import com.vlocity.quality.automation.ps.webapp.pagesimpl.LoginPageImpl;


public class LoginTest {
	private WebDriver driver;
//	private HomePage homePage; 
	
	/*
	@BeforeTest
	public void init() {
		driver = new FirefoxDriver();
	}
	
	@AfterTest
	public void cleanup(){
		if(driver != null) {
			driver.close();
		}
	}*/

	@Test(enabled=false)
	public void verify_that_login_succeeds_with_valid_credentials() {
		driver.get("https://login.salesforce.com");		
		LoginPage loginPage = new LoginPageImpl(driver);
		loginPage.loginAs("psv4@vlocity.com", "425Market");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertTrue(driver.getTitle().startsWith("Force.com Home Page"), "Login did not succeed");

//		homePage = new HomePageImpl(driver);
	}

	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"},enabled=false)
	public void verify_that_PublicSectorApp_is_selected_upon_login(){
		String appLabel = driver.findElement(By.id("tsidLabel")).getText();
		System.out.println("App Label : " + appLabel);
		
		Assert.assertTrue(appLabel.equalsIgnoreCase("Public Sector"), "Public Sector App is not the app selected");		
	}
	
/*	
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"})
	public void verify_that_all_expected_tabs_are_displayed(){
		// Assert that the tabs match the expected list
		System.out.println("List of Tabs");
		System.out.println("Number of Tabs :: "
				+ homePage.getTabLinkMap().keySet().size());
		
		for (String tabname : homePage.getTabLinkMap().keySet()) {
			System.out.println(tabname + " :: " + homePage.getTabLinkMap().get(tabname));
		}
	}
	
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"})
	public void verify_that_user_can_navigate_to_client_Tab(){
		homePage.switchToTab(FinancialServicesTabs.Clients);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
//		/html/body/div/div[2]/table/tbody/tr/td[2]/form/span/div/div/div[1]/table/tbody/tr/td[1]/h2
		
		WebElement mainTitle = driver.findElement(By.xpath("//td[@id='bodyCell']//td[@class='pbTitle']/h2[@class='mainTitle']"));
		Assert.assertEquals(mainTitle.getText(), "Clients", "Clients Tab did not open correctly");
		
	}
	
*/	
	/*
	@Test(dependsOnMethods = {"verify_that_login_succeeds_with_valid_credentials"})
	public void verify_that_Clients_Tab_is_displayed_correctly(){
		
		
		
	}*/
}
